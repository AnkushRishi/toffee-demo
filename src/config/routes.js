
import Welcome from "../containers/welcome";
import Loader from "../components/common/Loader";
import Screen1 from "../containers/screen1";
import Screen2 from "../containers/screen2";
import Screen3 from "../containers/screen3";
// export list of routes.
export default routes = {
	Loader : { screen: Loader },
	Welcome	: { screen: Welcome },
	Screen1 : { screen: Screen1 },
	Screen2 : { screen: Screen2 },
	Screen3 : { screen: Screen3 }
};
