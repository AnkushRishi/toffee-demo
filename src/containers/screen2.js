/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import NavigationBar from 'react-native-navbar';
import Constants from '../constants';
import Button from '../components/common/FormSubmitButton';
import Icon from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class Screen2 extends Component<Props> {
  constructor(props){
    super(props);
    this.state={
      checked:false,
      isCircleOneChecked:true,
      isCircleTwoChecked:true,
      isCircleThreeChecked:true,
      isCircleFourChecked:true,
      isDateTimePickerVisible: false,
      text1:'',
      text2:''
    }
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', date);
    this._hideDateTimePicker();
  };

  checked(){
    this.setState({
      checked:!this.state.checked
    })
  }

  circleOne(){
    this.setState({
      isCircleOneChecked:false,
      isCircleTwoChecked:true
    })
  }

  circleTwo(){
    this.setState({
      isCircleTwoChecked:false,
      isCircleOneChecked:true
    })
  }

  circleThree(){
    this.setState({
      isCircleThreeChecked:false,
      isCircleFourChecked:true
    })
  }

  circleFour(){
    this.setState({
      isCircleFourChecked:false,
      isCircleThreeChecked:true
    })
  }

  navigateToThirdScreen(){
    this.props.navigation.navigate('Screen3')
  }

  render() {
    return (
      <View style={styles.container}>
        <NavigationBar
          title={<Image source={Constants.Images.user.toffee_icon} style={styles.toffeeIcon} resizeMode={'contain'}></Image>}
          style={{backgroundColor:Constants.Colors.ToffeeNavbar,height:Constants.BaseStyle.DEVICE_HEIGHT/100*12}}
          statusBar={{hidden:true}}
        />
        <ScrollView>
          <View style={styles.subContainer}>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={()=>this.checked()} style={{flex:.1}}>
                <Image source={this.state.checked ? Constants.Images.user.checked : Constants.Images.user.uncheck} style={styles.checkbox} resizeMode={'contain'} />
              </TouchableOpacity>
              <Text style={styles.dummyText}>loreum ipsum1loreum ipsum1</Text>
            </View>
            <Text style={{marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*2,fontSize:16}}>Loreum ipsum loreum ipsum loreu23?</Text>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={()=>this.circleOne()} style={{flex:.2}}>
                <Image source={this.state.isCircleOneChecked ? Constants.Images.user.circle : Constants.Images.user.dotCircle} style={styles.circle} resizeMode={'contain'} />
              </TouchableOpacity>
              <View style={{flex:.8,marginTop: Constants.BaseStyle.DEVICE_HEIGHT/100*3.5}}>
                <Text style={{color:Constants.Colors.Black}}>Loreum ipsum loreum ipsum loreum ips1</Text>
                <TextInput 
                  onChangeText={(text1) => this.setState({text1})}
                  placeholder={'Loreum ipsum loreum ipsum loreum ips133'}
                  style={{height:Constants.BaseStyle.DEVICE_HEIGHT/100*5,borderBottomWidth:1}} />
              </View>
            </View>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={()=>this.circleTwo()} style={{flex:.2}}>
                <Image source={this.state.isCircleTwoChecked ? Constants.Images.user.circle : Constants.Images.user.dotCircle} style={styles.circle} resizeMode={'contain'} />
              </TouchableOpacity>
              <View style={{flex:.8,marginTop: Constants.BaseStyle.DEVICE_HEIGHT/100*3.5}}>
                <Text style={{color:Constants.Colors.Black}}>Loreum ipsum loreum ipsum loreum ips2</Text>
                <TextInput 
                  onChangeText={(text2) => this.setState({text2})}
                  placeholder={'Loreum ipsum loreum ipsum loreum ips100'}
                  style={{height:Constants.BaseStyle.DEVICE_HEIGHT/100*5,borderBottomWidth:1}} />
              </View>
            </View>
            <Text style={{marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*2,fontSize:16}}>Loreum ipsum loreum ipsum loreum ips1234?</Text>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={()=>this.circleThree()} style={{flex:.2}}>
                <Image source={this.state.isCircleThreeChecked ? Constants.Images.user.circle : Constants.Images.user.dotCircle} style={styles.circle} resizeMode={'contain'} />
              </TouchableOpacity>
              <Text style={styles.dummyTextWithCircle}>Yes</Text>
              <TouchableOpacity onPress={()=>this.circleFour()} style={{flex:.2}}>
                <Image source={this.state.isCircleFourChecked ? Constants.Images.user.circle : Constants.Images.user.dotCircle} style={styles.circle} resizeMode={'contain'} />
              </TouchableOpacity>
              <Text style={styles.dummyTextWithCircle}>No</Text>
            </View>
            <Text style={{marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*2,fontSize:16}}>Loreum ipsum lor345</Text>
            <View style={{flexDirection:'row',marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*2}}>
              <View style={{flex:.4}}>
                <TouchableOpacity onPress={this._showDateTimePicker} style={styles.datePickerView}>
                  <Text style={{textAlign:'center'}}>dd/mm/yyyy</Text>
                </TouchableOpacity>
                <DateTimePicker
                  isVisible={this.state.isDateTimePickerVisible}
                  onConfirm={this._handleDatePicked}
                  onCancel={this._hideDateTimePicker}
                />
              </View>
              <Text style={{flex:.2,textAlign:'center'}}>to</Text>
              <View style={{flex:.4}}>
                <TouchableOpacity onPress={this._showDateTimePicker} style={styles.datePickerView}>
                  <Text style={{textAlign:'center'}}>dd/mm/yyyy</Text>
                </TouchableOpacity>
                <DateTimePicker
                  isVisible={this.state.isDateTimePickerVisible}
                  onConfirm={this._handleDatePicked}
                  onCancel={this._hideDateTimePicker}
                />
              </View>
            </View>
            <Text style={{marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*4,fontSize:16}}>Loreum ipsum123</Text>
            <View style={{flexDirection:'row',marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*4}}>
              <View style={{flex:1,alignItems:'center'}}>
                <Icon name={'upload'} size={40} color={Constants.Colors.ToffeeButtonColor} />
                <Text>loreum ipsum1</Text>
              </View>
              <View style={{flex:1,alignItems:'center'}}>
                <Icon name={'upload'} size={40} color={Constants.Colors.ToffeeButtonColor} />
                <Text>loreum ip34</Text>
              </View>
              <View style={{flex:1,alignItems:'center'}}>
                <Icon name={'upload'} size={40} color={Constants.Colors.ToffeeButtonColor} />
                <Text>loreum ipsum4</Text>
              </View>
            </View>
            <Button text={'loreum ipsum'} _Press={()=>this.navigateToThirdScreen()} buttonStyle={{marginVertical:Constants.BaseStyle.DEVICE_HEIGHT/100*4}} />  
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Constants.Colors.White
  },
  toffeeIcon:{
    height: Constants.BaseStyle.DEVICE_HEIGHT/100*17,
    width: Constants.BaseStyle.DEVICE_WIDTH/100*25,
    top: Constants.BaseStyle.DEVICE_HEIGHT/100*5,
    alignSelf:'center'
  },
  subContainer:{
    flex:1,
    marginHorizontal: Constants.BaseStyle.DEVICE_WIDTH/100*5,
  },
  dummyText:{
    flex:.9,
    color: Constants.Colors.LightBlack,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT/100*2.5,
  },
  checkbox:{
    height: Constants.BaseStyle.DEVICE_HEIGHT/100*8,
    width: Constants.BaseStyle.DEVICE_WIDTH/100*8,
  },
  circle:{
    height: Constants.BaseStyle.DEVICE_HEIGHT/100*10,
    width: Constants.BaseStyle.DEVICE_WIDTH/100*10,
  },
  dummyTextWithCircle:{
    flex:.8,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT/100*3.5,
    color:Constants.Colors.Black
  },
  datePickerView:{
    borderBottomWidth:1
  }
});
