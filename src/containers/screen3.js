/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import NavigationBar from 'react-native-navbar';
import Constants from '../constants';
import Button from '../components/common/FormSubmitButton';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Screen3 extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <NavigationBar
          title={<Image source={Constants.Images.user.toffee_icon} style={styles.toffeeIcon} resizeMode={'contain'}></Image>}
          style={{backgroundColor:Constants.Colors.ToffeeNavbar,height:Constants.BaseStyle.DEVICE_HEIGHT/100*12}}
          statusBar={{hidden:true}}
        />
        <View style={styles.subContainer}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
            <View style={{flex:.4,alignItems:'flex-end'}}>
              <Icon name={'check-circle'} size={60} color={'rgb(95,226,230)'} />
            </View>
            <Text style={{flex:.6,marginLeft:Constants.BaseStyle.DEVICE_WIDTH/100*5,fontSize:16}}>loreum ipsum00</Text>
          </View>
          <View style={{flexDirection:'row',alignItems:'center',marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*5}}>
            <View style={{flex:.4,alignItems:'flex-end'}}>
              <Icon name={'redo-alt'} size={60} color={'rgb(95,226,230)'} />
            </View>
            <Text style={{flex:.6,marginLeft:Constants.BaseStyle.DEVICE_WIDTH/100*5,fontSize:16}}>loreum ipsum00</Text>
          </View>
          <View style={{flexDirection:'row',alignItems:'center',marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*5}}>
            <View style={{flex:.4,alignItems:'flex-end'}}>
              <Icon name={'circle'} size={60} color={Constants.Colors.BlurGrey} />
            </View>
            <Text style={{flex:.6,marginLeft:Constants.BaseStyle.DEVICE_WIDTH/100*5,fontSize:16}}>loreum ipsum00</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Constants.Colors.White
  },
  toffeeIcon:{
    height: Constants.BaseStyle.DEVICE_HEIGHT/100*17,
    width: Constants.BaseStyle.DEVICE_WIDTH/100*25,
    top: Constants.BaseStyle.DEVICE_HEIGHT/100*5,
    alignSelf:'center'
  },
  subContainer:{
    flex:1,
    alignItems:'center',
    //justifyContent:'center'
    marginTop:Constants.BaseStyle.DEVICE_HEIGHT/100*20,
  },
  dummyText:{
    color:Constants.Colors.LightBlack,
    textAlign:'center',
    marginVertical:Constants.BaseStyle.DEVICE_HEIGHT/100*15
  }
});
