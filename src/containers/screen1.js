/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import NavigationBar from 'react-native-navbar';
import Constants from '../constants';
import Button from '../components/common/FormSubmitButton';

export default class Screen1 extends Component<Props> {
  navigateToSecondScreen(){
    console.log('to screen 2 ***** ')
    this.props.navigation.navigate('Screen2')
  }

  render() {
    return (
      <View style={styles.container}>
        <NavigationBar
          title={<Image source={Constants.Images.user.toffee_icon} style={styles.toffeeIcon} resizeMode={'contain'}></Image>}
          style={{backgroundColor:Constants.Colors.ToffeeNavbar,height:Constants.BaseStyle.DEVICE_HEIGHT/100*12}}
          statusBar={{hidden:true}}
        />
        <View style={styles.subContainer}>
          <Text style={styles.dummyText}>loreum ipsum1loreum ipsum1</Text>
          <Button text={'loreum ipsum()'} _Press={()=>this.navigateToSecondScreen()} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Constants.Colors.White
  },
  toffeeIcon:{
    height: Constants.BaseStyle.DEVICE_HEIGHT/100*17,
    width: Constants.BaseStyle.DEVICE_WIDTH/100*25,
    top: Constants.BaseStyle.DEVICE_HEIGHT/100*5,
    alignSelf:'center'
  },
  subContainer:{
    flex:1,
    alignItems:'center',
    //justifyContent:'center'
  },
  dummyText:{
    color:Constants.Colors.LightBlack,
    textAlign:'center',
    marginVertical:Constants.BaseStyle.DEVICE_HEIGHT/100*15
  }
});
