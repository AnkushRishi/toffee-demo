'use strict';
module.exports = {
	user:{
		toffee_icon : require("../assets/images/toffee_icon.png"),
		uncheck : require("../assets/images/uncheck.png"),
		checked : require("../assets/images/checked.png"),
		circle : require("../assets/images/circle.png"),
		dotCircle : require("../assets/images/dotCircle.png"),
	}
};
